to install base panel run

    kpackagetool5 --type=Plasma/LayoutTemplate -i $PWD


to upgrade base panel run

    kpackagetool5 --type=Plasma/LayoutTemplate -u $PWD


to remove panel run

    kpackagetool5 --type=Plasma/LayoutTemplate -r $(grep X-KDE-PluginInfo-Name metadata.desktop | cut -d '=' -f2)
